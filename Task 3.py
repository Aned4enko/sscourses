# 1
a = 'I am testing'
print(a[::-1])
# 2
for i in reversed('I am testing'):
    print(i, end="")
print()
#3


def reverse(text):
    return text[::-1]
print(reverse('I am testing'))

