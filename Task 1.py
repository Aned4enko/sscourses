a = 'World'
# 1
print("Hello, ", a, "!", sep='')
# 2
print("Hello, {}!".format(a))
# 3
print("Hello, %s!" % "World")
# 4
print(f"Hello, {a}!")
